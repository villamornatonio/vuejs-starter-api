<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$router->post('auth/token', 'Api\AuthController@authenticate');
$router->post('auth/personalToken', 'Api\AuthController@authenticatePersonalToken');
$router->post('auth/refresh', 'Api\AuthController@refreshToken');
$router->get('user', 'Api\AuthController@getUser')->middleware('auth:api');

$router->group(['prefix' => 'v1/'], function ($router) {
    $router->group(['prefix' => 'users'], function ($router) {
        $router->get('/', 'Api\UsersController@index');
    });
});
