## Setting Up Laravel on Heroku
 - Create an account on heroku
 - Create an app in Heroku (PHP) US is much closer in the PH
 - download Heroku Tool Belt - https://devcenter.heroku.com/articles/heroku-cli#macos
 - Install Heroku Tool Belt
 - heroku login      // bring up terminal and login to heruko tool belt
 - Enter Email // From Heroku
 - Enter Password // of course from your register heroku account
 - create a repo on bit bucket
 - clone the repo on your local
 - initialize the project and push to bitbucket and you now have a repo with barebones files
 - heroku git:remote -a HEROKU_APP_NAME //replace the HEROKU_APP_NAME from your reroku app that you created

## Setting up Profile on Heroku
 - Laravel has a file public in which case you want to point heroku server to initialize or run there
 - touch Procfile // run this on the root directory of your laravel project
 - web: vendor/bin/heroku-php-apache2 public/    <-- add that text on the Procfile that you have
 - commit the changes and push to your repo and to heroku
 
## Setting Up Database on Heroku (PostgreSQL)
 - PostgreSQL or clearDB MySQL
 - clearDB MySQL requires Credit Card to setup although its free so just use PostgreSQL anyways they are most likely similar only if you just use simpel sql queries then it should be fine
 - go to your heroku app on the browser
 - select the app that you add a database on the browser
 - click the "Resources" Tab 
 - when you see the Add-Ons search for PostgreSQL
 - it should have it there and click it
 - select a free version
 - click provision and it will now add a database on your app
 - you can see on the terminal of the app there is a config added
 - to see the config go to the terminal and enter 
 - heroku config -a HEROKU_APP_NAME //replace the HEROKU_APP_NAME from your reroku app that you created
 - modify the config/database.php
  <code>
 
  $pgsql = [
      'driver' => 'pgsql',
      'host' => env('DB_HOST', '127.0.0.1'),
      'port' => env('DB_PORT', '5432'),
      'database' => env('DB_DATABASE', 'forge'),
      'username' => env('DB_USERNAME', 'forge'),
      'password' => env('DB_PASSWORD', ''),
      'charset' => 'utf8',
      'prefix' => '',
      'schema' => 'public',
      'sslmode' => 'prefer',
  ];
  if (env('APP_ENV') == 'heroku') {
  
      $pgsql = [
          'driver'   => 'pgsql',
          'host'     => parse_url(getenv("DATABASE_URL"))["host"],
          'database' => substr(parse_url(getenv("DATABASE_URL"))["path"], 1),
          'username' => parse_url(getenv("DATABASE_URL"))["user"],
          'password' => parse_url(getenv("DATABASE_URL"))["pass"],
          'charset'  => 'utf8',
          'prefix'   => '',
          'schema'   => 'public',
      ];
  
  }




 'pgsql' => $pgsql,
  </code>
 
## Setting Up Enviroment Varialbles in Heroku
 - on other deployments we use .env but in heruko we dont have to we just have to set variables
 - heroku config -a HEROKU_APP_NAME //replace the HEROKU_APP_NAME from your reroku app that you created
 - heroku config:set APP_ENV=heroku -a HEROKU_APP_NAME
 - heroku config:set APP_KEY=thisisthekeythatyougetonrunningphpartisankeygenerate -a HEROKU_APP_NAME
 - heroku config:set APP_NAME=thisisthetitlethatyouwantforyourapp -a HEROKU_APP_NAME
 - heroku config:set APP_DEBUG=false -a HEROKU_APP_NAME
 - heroku config:set APP_URL=https://thedomainthatherokuwillprovideyou/ -a HEROKU_APP_NAME
 - heroku config:set DB_CONNECTION=pgsql -a HEROKU_APP_NAME
 - heroku config -a HEROKU_APP_NAME //replace the HEROKU_APP_NAME from your reroku app that you created
 - and now you can see the variables that you have set
 
## Migrating and Seeding on Heroku
 - After you setup your Environment Variables and setup your database 
 - lets migrate and see the DB
 - heroku run php artisan migrate -a HEROKU_APP_NAME
 - heroku run php artisan db:seed -a HEROKU_APP_NAME
 - now you will see the migration and seeding happening


## Heroku Deployment

Pushing to Heroku
 - git commit -am "UDPATE"
 - git push origin development
 - git checkout staging
 - git merge development
 - git commit -am "UPDATE"
 - git push origin staging
 - git checkout master 
 - git merge staging
 - git commit -am "UPDATE"
 - git push origin master
 - git push heroku master
 


