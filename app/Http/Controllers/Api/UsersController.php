<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Transformers\UsersTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class UsersController
 * @package App\Http\Controllers\Api
 */
class UsersController extends ApiController
{
    /**
     * @var UsersTransformer
     */
    private $usersTransformer;

    /**
     * UsersController constructor.
     * @param UsersTransformer $usersTransformer
     */
    public function __construct(UsersTransformer $usersTransformer)
    {
        $this->usersTransformer = $usersTransformer;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::all();

        $data = $this->usersTransformer->transformCollection($users->toArray());


        return $this->respond('Suceess', $data);
    }
}
