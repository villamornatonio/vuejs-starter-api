<?php

namespace App\Transformers;

/**
 * Class UsersTransformer
 * @package App\Transformers
 */
class UsersTransformer extends Transformer
{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($item)
    {
        return [
            'id' => $item['id'],
            'first_name' => $item['first_name'],
            'last_name' => $item['last_name'],
            'email' => $item['email'],
            'name' => ucfirst($item['first_name']) . ' ' . ucfirst($item['last_name']),
        ];
    }


}

