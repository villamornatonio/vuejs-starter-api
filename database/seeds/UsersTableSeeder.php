<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [   'first_name' => 'Admin',
                'last_name' => 'Admin',
                'email' => 'admin@matchup.com',
                'password' => Hash::make('password'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [   'first_name' => 'Partner',
                'last_name' => 'Partner',
                'email' => 'partner@matchup.com',
                'password' => Hash::make('password'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
            [   'first_name' => 'User',
                'last_name' => 'User',
                'email' => 'user@matchup.com',
                'password' => Hash::make('password'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ],
        ];

        DB::table('users')->insert($users);
    }
}
